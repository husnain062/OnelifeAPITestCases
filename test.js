var randomstring = require("randomstring");
var request = require('request');
var assert = require('assert');
var should = require('should');
var mocha = require('mocha')
var rn = require('random-number');
var Fakerator = require("fakerator");
var fakerator = Fakerator("de-DE");
var express = require('express');
var app = express();
var date = '01/16/2018'
var start_date = '01/10/2018'
var end_date = '01/16/2018'
var user_id;
var user_journalid;


var URL = 'http://132.148.133.186:3008';
var password = fakerator.internet.password(8)
var username = fakerator.internet.userName()
var email = fakerator.internet.email()
var fb_id = fakerator.misc.uuid()
var used_email = 'johnthuneoz+123@gmail.com'
var invalid_email = 'johnthuneoz+123gmail.com'
var empty_email = '';
var empty_password = '';
var empty_username = '';
var used_username = 'johnthuneoz123'
var invalid_password = fakerator.internet.password(7)
var deviceId = '1'
var deviceType = '1'
var access_token;
var Verification_code;
var exercise;
var water;
var custom_foodid;

// Test Cases for Sign Up API 
describe('Test Cases of SignUp API', function(){
    this.timeout(20000);
    it("It should return 200 if user is SignUp successfully", (done)=>{
        request.post({url : URL+'/onelife/signup', form: {email : email, password : password , deviceId : deviceId, deviceType : deviceType,username : username}}, (err, response)=>{
            if(err){
                done("Error in SignUp Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                access_token = res.response.access_token
                assert.equal(res.responseCode, 200, "SignUp successfully");
                res.response.should.have.property('UserID').which.is.a.Number();
                done();
            }
        });
    });
    it("It should return 208 if user enter an email which is already registered", (done)=>{
        request.post({url : URL+'/onelife/signup', form: {email : used_email, password : password , deviceId : deviceId, deviceType : deviceType,username : username}}, (err, response)=>{
            if(err){
                done("Error in SignUp Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 208, "This email address is already in use. Please try another.");
                done();
            }
        });
    });
    it("It should return 401 if user enter an invalid email", (done)=>{
        request.post({url : URL+'/onelife/signup', form: {email : invalid_email, password : password , deviceId : deviceId, deviceType : deviceType,username : username}}, (err, response)=>{
            if(err){
                done("Error in SignUp Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "Please enter a valid email");
                done();
            }
        });
    });
    it("It should return 206 if email is empty", (done)=>{
        request.post({url : URL+'/onelife/signup', form: {email : empty_email,password : password , deviceId : deviceId, deviceType : deviceType,username : username}}, (err, response)=>{
            if(err){
                done("Error in SignUp Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 206, "Required Param missing");
                done();
            }
        });
    });
    // it("It should return 206 if username is empty", (done)=>{
    //     request.post({url : URL+'/onelife/signup', form: {email : email,password : password , deviceId : deviceId, deviceType : deviceType}}, (err, response)=>{
    //         if(err){
    //             done("Error in SignUp Test Case", err);
    //         }
    //         else{
    //             let res = JSON.parse(response.body);
    //             assert.equal(res.responseCode, 206, "Required Param missing");
    //             done();
    //         }
    //     });
    // });
    it("It should return 208 if username already in use", (done)=>{
        request.post({url : URL+'/onelife/signup', form: {email : email,password : empty_password , deviceId : deviceId, deviceType : deviceType,username : used_username}}, (err, response)=>{
            if(err){
                done("Error in SignUp Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 208, "Required Param missing");
                done();
            }
        });
    });
    //   it("It should return 206 if password is empty", (done)=>{
    //     request.post({url : URL+'/onelife/signup', form: {email : email,password : empty_password , deviceId : deviceId, deviceType : deviceType,username : username}}, (err, response)=>{
    //         if(err){
    //             done("Error in SignUp Test Case", err);
    //         }
    //         else{
    //             let res = JSON.parse(response.body);
    //             assert.equal(res.responseCode, 206, "Required Param missing");
    //             done();
    //         }
    //     });
    // });
    it("It should return 208 if password length is less than 8", (done)=>{
        request.post({url : URL+'/onelife/signup', form: {email : email,password : invalid_password , deviceId : deviceId, deviceType : deviceType,username : username}}, (err, response)=>{
            if(err){
                done("Error in SignUp Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 208, "Required Param missing");
                done();
            }
        });
    });
    it("It should return 206 if Username,Email or password is missing", (done)=>{
        request.post({url : URL+'/onelife/signup', form: {email : empty_email,username : empty_username,password : empty_password , deviceId : deviceId, deviceType : deviceType}}, (err, response)=>{
            if(err){
                done("Error in SignUp Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 206, "Required Param missing");
                done();
            }
        });
    });
    });
//Test Cases of Set Profile API
describe('Test Cases of Update Profile API', function(){
    this.timeout(20000);
    it("It should return 201 if profile is set sucessfully", (done)=>{
        request.put({url : URL+'/onelife/updateprofile',  headers : {access_token : access_token},form: {start_weight : '200',goal_weight : '100',gender:"Female",height_inches:'5',height_feet:'5',dob:'01-02-1986',daily_blocks:'23'}}, (err, response)=>{
            if(err){
                done("Error in Profile Update Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 201, "Updated User Profile");
                done();
            }
        });
    });

    });
//Test cases of Facebook Api
describe('Test Cases of Facebook API', function(){
    this.timeout(20000);
    it("It should return 200 if user is SignUp successfully through Facebook", (done)=>{
        request.post({url : URL+'/onelife/facebook',form: {fb_id : fb_id}}, (err, response)=>{
            if(err){
                done("Error in SignUp with facebook Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                 //access_token = res.response.access_token
                assert.equal(res.responseCode, 200, "User SignUp Sucessfully");
                done();
            }
        });
    });
    it("It should return 200 if user is Signin successfully through Facebook", (done)=>{
        request.post({url : URL+'/onelife/facebook', form: {fb_id : 1}}, (err, response)=>{
            if(err){
                done("Error in Login with facebook Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                //access_token = res.response.access_token
                assert.equal(res.responseCode, 200, "User Login Sucessfully");
                done();
            }
        });
    });
    it("It should return 206 if facebook ID is left empty", (done)=>{
        request.post({url : URL+'/onelife/facebook', form: {fb_id : ''}}, (err, response)=>{
            if(err){
                done("Error in SignUp with facebook Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                //access_token = res.response.access_token
                assert.equal(res.responseCode, 206, "Empty Facebook ID");
                done();
            }
        });
    });
    });
describe('Test Cases of Forgot Password API', function(){
    this.timeout(20000);
    it("It should return 200 if email is valid", (done)=>{
        request.post({url : URL+'/onelife/forgetpassword', form: {email : email}}, (err, response)=>{
            if(err){
                done("Error in forgot password Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                Verification_code = res.response.code;
                assert.equal(res.responseCode, 200, "Verification code sent to your email");
                done();
            }
        });
    });
    it("It should return 204 if email is empty", (done)=>{
        request.post({url : URL+'/onelife/forgetpassword', form: {email : ''}}, (err, response)=>{
            if(err){
                done("Error in forgot password Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 204, "Please Enter Email");
                done();
            }
        });
    });
    it("It should return 204 if email is invalid", (done)=>{
        request.post({url : URL+'/onelife/forgetpassword', form: {email : 'john'}}, (err, response)=>{
            if(err){
                done("Error in forgot password Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 204, "Please enter a valid email.");
                done();
            }
        });
    });
    it("It should return 204 if email is with spaces", (done)=>{
        request.post({url : URL+'/onelife/forgetpassword', form: {email : '      '}}, (err, response)=>{
            if(err){
                done("Error in forgot password Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 204, "Please enter a valid email.");
                done();
            }
        });
    });
    it("It should return 204 if Verification code is invalid", (done)=>{
        request.post({url : URL+'/onelife/forgetpassword', form: {email : email , code : '2555'}}, (err, response)=>{
            if(err){
                done("Error in forgot password Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 204, "Invalid reset code");
                done();
            }
        });
    });
    it("It should return 204 if Verification code is Empty", (done)=>{
        request.post({url : URL+'/onelife/forgetpassword', form: {email : email , code : ' '}}, (err, response)=>{
            if(err){
                done("Error in forgot password Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 204, "Invalid reset code");
                done();
            }
        });
    });
    it("It should return 200 if Verification code is valid", (done)=>{
        request.post({url : URL+'/onelife/forgetpassword', form: {email : email , code : Verification_code}}, (err, response)=>{
            if(err){
                done("Error in forgot password Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Code verified successfully");
                done();
            }
        });
    });
    it("It should return 200 if user click on userName", (done)=>{
        request.post({url : URL+'/onelife/forgetpassword', form: {email : email , code : Verification_code , key : 'userName'}}, (err, response)=>{
            if(err){
                done("Error in forgot password Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Username Found");
                done();
            }
        });
    });
    it("It should return 206 if user enter password less then 8 characters or left empty ", (done)=>{
        request.post({url : URL+'/onelife/forgetpassword', form: {email : email , code : Verification_code , key : 'proceed',password:''}}, (err, response)=>{
            if(err){
                done("Error in forgot password Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 206, "Please enter a password of minimum 8 characters");
                done();
            }
        });
    });
    it("It should return 200 if user enter valid password ", (done)=>{
        request.post({url : URL+'/onelife/forgetpassword', form: {email : email , code : Verification_code , key : 'proceed',password:'12345678'}}, (err, response)=>{
            if(err){
                done("Error in forgot password Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                password = '12345678'
                assert.equal(res.responseCode, 200, "Password changed successfully");
                done();
            }
        });
    });
    });
//Test Cases of SignIn API.
describe('Test Cases of SignIn API', function(){
    this.timeout(20000);
    it("It should return 200 if user SignIn using Username", (done)=>{
        request.post({url : URL+'/onelife/signin', form: {username : username,password:password}}, (err, response)=>{
            if(err){
                done("Error in SignIn API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                access_token = res.result.access_token
                user_id = res.result.UserID
                assert.equal(res.responseCode, 200, "User Login Sucessfully");
                done();
            }
        });
    });
    it("It should return 200 if user SignIn using email", (done)=>{
        request.post({url : URL+'/onelife/signin', form: {username : email,password:password}}, (err, response)=>{
            if(err){
                done("Error in SignIn API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                access_token = res.result.access_token
                assert.equal(res.responseCode, 200, "User Login Sucessfully");
                done();
            }
        });
    });
    it("It should return 401 if user left password empty", (done)=>{
        request.post({url : URL+'/onelife/signin', form: {username : email,password:''}}, (err, response)=>{
            if(err){
                done("Error in SignIn API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "Please enter the password");
                done();
            }
        });
    });
    it("It should return 401 if passwordis invalid", (done)=>{
        request.post({url : URL+'/onelife/signin', form: {username : email,password:'123456'}}, (err, response)=>{
            if(err){
                done("Error in SignIn API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "Password is invalid");
                done();
            }
        });
    });
    });

//Test Cases of Get Profile API.
describe('Test Cases of Get Profile API', function(){
    this.timeout(20000);
    it("It should return 401 if user is not authorized.", (done)=>{
        request.get({url : URL+'/onelife/getprofile', headers : {access_token : 'wfewfweweewfewf'}}, (err, response)=>{
            if(err){
                done("Error in Get Profile API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "You are not authorize user");
                done();
            }
        });
    });
    it("It should return 201 if get profile successfully.", (done)=>{
        request.get({url : URL+'/onelife/getprofile', headers : {access_token : access_token}}, (err, response)=>{
            if(err){
                done("Error in Get Profile API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 201, "User Profile get successfully");
                done();
            }
        });
    });
    });

//Test Cases of Update Profile API.
describe('Test Cases of Update Profile API', function(){
    this.timeout(20000);
    it("It should return 201 if profile updated successfully.", (done)=>{
        request.put({url : URL+'/onelife/updateprofile', headers : {access_token : access_token}, form : {start_weight :'250', goal_weight : '150', gender : 'Male', height_inches: '7', height_feet: '5', dob : '01-02-1996' , daily_blocks : '25'}}, (err, response)=>{
            if(err){
                done("Error in Update Profile API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 201, "Updated User Profile");
                done();
            }
        });
    });
    it("It should return 401 if profile does not updated successfully.", (done)=>{
        request.put({url : URL+'/onelife/updateprofile', headers : {access_token : 'rghkjnskjgfnknkfnlke'}, form : {start_weight :'250', goal_weight : '150', gender : 'Male', height_inches: '7', height_feet: '5', dob : '01-02-1996' , daily_blocks : '25'}}, (err, response)=>{
            if(err){
                done("Error in Update Profile API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "You are not authorize user");
                done();
            }
        });
    });
    });

//Test Cases of Get Food Entries API.
describe('Test Cases of Get Food Entries API', function(){
    this.timeout(20000);
    it("It should return 200 if Food Entries get successfully", (done)=>{
        request.post({url : URL+'/onelife/getfoodentries', headers : {access_token : access_token}, form : {value : 'coffee', id : 25451}}, (err, response)=>{
            if(err){
                done("Error in Get Food Enteries API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record Fetch Sucessfully");
                if(res.response.should.not.be.empty()){
                    res.response[0].should.have.property('FoodEntryID').which.is.a.Number();
                    res.response[0].should.have.property('FoodName').which.is.a.String();
                    res.response[0].should.have.property('FoodBlocks').which.is.a.Number();
                    res.response[0].should.have.property('FreeFood').which.is.a.Boolean();
                    res.response[0].should.have.property('Carbs').which.is.a.Number();
                    res.response[0].should.have.property('servings').which.is.a.Number();
                    res.response[0].should.have.property('DValue').which.is.a.Boolean();
                    res.response[0].should.have.property('ServingSize').which.is.a.String();
                }
                done();
            }
        });
    });
    it("It should return 401 if access token is invalid.", (done)=>{
        request.post({url : URL+'/onelife/getfoodentries', headers : {access_token : 'rbdgjkbskjnfkjnsdjk'}, form : {value : 'coffee', id : 25451}}, (err, response)=>{
            if(err){
                done("Error in Get Food Enteries API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "You are not authorize user");
                done();
            }
        });
    });
    it("It should return 200 if Food Entries get successfully", (done)=>{
        request.post({url : URL+'/onelife/getfoodentries', headers : {access_token : access_token}, form : {value : 'coffee', id : 25451, category : 'free_food'}}, (err, response)=>{
            if(err){
                done("Error in Get Food Enteries API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record Fetch Sucessfully");
                if(res.response.should.not.be.empty()){
                    res.response[0].should.have.property('FoodEntryID').which.is.a.Number();
                    res.response[0].should.have.property('FoodName').which.is.a.String();
                    res.response[0].should.have.property('FoodBlocks').which.is.a.Number();
                    assert.equal(res.response[0].FreeFood, true, 'It is a Free Food');
                    res.response[0].should.have.property('Carbs').which.is.a.Number();
                    res.response[0].should.have.property('servings').which.is.a.Number();
                    res.response[0].should.have.property('DValue').which.is.a.Boolean();
                    res.response[0].should.have.property('ServingSize').which.is.a.String();
                }
                done();
            }
        });
    });

        it("It should return 200 if Food Entries get successfully", (done)=>{
        request.post({url : URL+'/onelife/getfoodentries', headers : {access_token : access_token}, form : {value : 'coffee', id : 25451, category : 'carbs'}}, (err, response)=>{
            if(err){
                done("Error in Get Food Enteries API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record Fetch Sucessfully");
                if(res.response.should.not.be.empty()){
                    res.response[0].should.have.property('FoodEntryID').which.is.a.Number();
                    res.response[0].should.have.property('FoodName').which.is.a.String();
                    res.response[0].should.have.property('FoodBlocks').which.is.a.Number();
                    res.response[0].should.have.property('FreeFood').which.is.a.Boolean();
                    assert.equal(res.response[0].Carbs, 1, 'It is a Carb Free Food');
                    res.response[0].should.have.property('servings').which.is.a.Number();
                    res.response[0].should.have.property('DValue').which.is.a.Boolean();
                    res.response[0].should.have.property('ServingSize').which.is.a.String();
                }
                done();
            }
        });
    });

        it("It should return 200 if Food Entries get successfully", (done)=>{
        request.post({url : URL+'/onelife/getfoodentries', headers : {access_token : access_token}, form : {value : 'Yeast', id : 25451, category : 'free_food'}}, (err, response)=>{
            if(err){
                done("Error in Get Food Enteries API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record Fetch Sucessfully");
                if(res.response.should.not.be.empty()){
                    res.response[0].should.have.property('FoodEntryID').which.is.a.Number();
                    res.response[0].should.have.property('FoodName').which.is.a.String();
                    res.response[0].should.have.property('FoodBlocks').which.is.a.Number();
                    assert.equal(res.response[0].Carbs, 0, 'It is a Carb Free Food');
                    assert.equal(res.response[0].FreeFood, true, 'It is a Free Food');
                    res.response[0].should.have.property('servings').which.is.a.Number();
                    res.response[0].should.have.property('DValue').which.is.a.Boolean();
                    res.response[0].should.have.property('ServingSize').which.is.a.String();
                }
                done();
            }
        });
    });

        it("It should return 200 if Food Entries get successfully", (done)=>{
        request.post({url : URL+'/onelife/getfoodentries', headers : {access_token : access_token}, form : {value : 'Bacon, raw', id : 25451, category : 'carbs'}}, (err, response)=>{
            if(err){
                done("Error in Get Food Enteries API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record Fetch Sucessfully");
                if(res.response.should.not.be.empty()){
                    res.response[0].should.have.property('FoodEntryID').which.is.a.Number();
                    res.response[0].should.have.property('FoodName').which.is.a.String();
                    res.response[0].should.have.property('FoodBlocks').which.is.a.Number();
                    assert.equal(res.response[0].Carbs, 1, 'It is a Carb Free Food');
                    assert.equal(res.response[0].FreeFood, false, 'It is a Free Food');
                    res.response[0].should.have.property('servings').which.is.a.Number();
                    res.response[0].should.have.property('DValue').which.is.a.Boolean();
                    res.response[0].should.have.property('ServingSize').which.is.a.String();
                }
                done();
            }
        });
    });
});

//Test Cases of Add to Journal API.
describe('Test Cases of Add to Journal API', function(){
    this.timeout(20000);
    it("It should return 200 if food add to journal successfully", (done)=>{
        request.post({url : URL+'/onelife/addjournal', headers : {access_token : access_token}, form : {date : date , foodId : 1069, blocks : 2, is_custom : 0, is_favourite : 0, id : user_id , is_public : 0, customfood : 0, category_id : 3, serving : 7}}, (err, response)=>{
            if(err){
                done("Error in Add to Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record Fetch Sucessfully");
                done();
            }
        });
    });

    it("It should return 200 if food add to journal successfully", (done)=>{
        request.post({url : URL+'/onelife/addjournal', headers : {access_token : access_token}, form : {date : date , foodId : 1069, blocks : 2, is_custom : 0, is_favourite : 0, id : user_id , is_public : 0, customfood : 0, category_id : 3, serving : 7}}, (err, response)=>{
            if(err){
                done("Error in Add to Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record fetch successfully");
                done();
            }
        });
    });
});

// Test Cases for Water, Exercise, Notes
describe('Test Cases of Add Exercise API', function(){
    this.timeout(20000);
    it("It should return 200 if exercise added successfully", (done)=>{
        request.post({url : URL+'/onelife/userattribute', headers : {access_token : access_token}, form : {id : user_id , date : date, exercise : 1}}, (err, response)=>{
            if(err){
                done("Error in Add Exercise API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record added successfully");
                done();
            }
        });
    });

    it("It should return 401 if access token is invalid", (done)=>{
        request.post({url : URL+'/onelife/userattribute', headers : {access_token : 'kjerhjfbkn'}, form : {id : user_id , date : date, exercise : 1}}, (err, response)=>{
            if(err){
                done("Error in Add Exercise API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "You are not authorize user");
                done();
            }
        });
    });

    it("It should return 206 if exercise already added", (done)=>{
        request.post({url : URL+'/onelife/userattribute', headers : {access_token : access_token}, form : {id : user_id , date : date, exercise : 1}}, (err, response)=>{
            if(err){
                done("Error in Add Exercise API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 206, "You have already done exercise today");
                done();
            }
        });
    });

    it("It should return 400 if any param missing", (done)=>{
        request.post({url : URL+'/onelife/userattribute', headers : {access_token : access_token}, form : {id : '' , date : date, exercise : 1}}, (err, response)=>{
            if(err){
                done("Error in Add Exercise API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 400, "Bad Request Error");
                done();
            }
        });
    });  

    it("It should return 200 if notes added successfully", (done)=>{
        request.post({url : URL+'/onelife/userattribute', headers : {access_token : access_token}, form : {id : user_id , date : date, note : 'I have eaten a lot today'}}, (err, response)=>{
            if(err){
                done("Error in Add Notes API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record added successfully");
                done();
            }
        });
    });  

    it("It should return 200 if water added successfully", (done)=>{
        request.post({url : URL+'/onelife/userattribute', headers : {access_token : access_token}, form : {id : user_id , date : date, water : '24'}}, (err, response)=>{
            if(err){
                done("Error in Add Notes API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record added successfully");
                done();
            }
        });
    });  

});
//Test Cases of Get Journal API.
describe('Test Cases of Get Journal API', function(){
    this.timeout(20000);
    it("It should return 200 if journal get successfully", (done)=>{
        request.post({url : URL+'/onelife/getjournal', headers : {access_token : access_token}, form : {date : date , id : user_id}}, (err, response)=>{
            if(err){
                done("Error in Get Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                user_journalid = res.response.Lunch[0].UserJournalID;
                exercise = res.response.Quickblocks[0].DidExercise;
                water = res.response.Quickblocks[0].WaterCount;
                assert.equal(res.responseCode, 200, "Record get successfully");
                done();
            }
        });
    });

        it("It should return 401 if access token is invalid", (done)=>{
        request.post({url : URL+'/onelife/getjournal', headers : {access_token : 'kltnghlmdflmglm'}, form : {date : date , id : user_id}}, (err, response)=>{
            if(err){
                done("Error in Get Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "You are not authorize user");
                done();
            }
        });
    });
});

//Test Cases of Update Journal API.
describe('Test Cases of Update Journal API', function(){
    this.timeout(20000);
    it("It should return 200 if journal get successfully", (done)=>{
        request.post({url : URL+'/onelife/updatejournal', headers : {access_token : access_token}, form : {foodId : 1069, id : user_id, blocks : 4, serving : 14}}, (err, response)=>{
            if(err){
                done("Error in Update Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Record updated successfully");
                done();
            }
        });
    });

        it("It should return 401 if access token is invalid", (done)=>{
        request.post({url : URL+'/onelife/updatejournal', headers : {access_token : 'kltnghlmdflmglm'}, form : {foodId : 1069, id : user_id, blocks : 4, serving : 14}}, (err, response)=>{
            if(err){
                done("Error in Update Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "You are not authorize user");
                done();
            }
        });
    });

        it("It should return 400 if any param missing", (done)=>{
        request.post({url : URL+'/onelife/updatejournal', headers : {access_token : access_token}, form : {foodId : '', id : user_id, blocks : 4, serving : 14}}, (err, response)=>{
            if(err){
                done("Error in Update Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 400, "Bad request error.");
                done();
            }
        });
    });
});

//Test Cases of Email Journal API.
describe('Test Cases of Email Journal API', function(){
    this.timeout(20000);
    it("It should return 200 if email journal sent successfully", (done)=>{
        request.post({url : URL+'/onelife/emailjournal', headers : {access_token : access_token}, form : {id : user_id, startdate : start_date, enddate : end_date}}, (err, response)=>{
            if(err){
                done("Error in Email Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Email journal sent successfully");
                done();
            }
        });
    });

        it("It should return 401 if access token is invalid", (done)=>{
        request.post({url : URL+'/onelife/emailjournal', headers : {access_token : 'kltnghlmdflmglm'}, form : {id : user_id, startdate : start_date, enddate : end_date}}, (err, response)=>{
            if(err){
                done("Error in Email Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "You are not authorize user");
                done();
            }
        });
    });

        it("It should return 400 if any param missing", (done)=>{
        request.post({url : URL+'/onelife/emailjournal', headers : {access_token : access_token}, form : {startdate : start_date, enddate : end_date}}, (err, response)=>{
            if(err){
                done("Error in Email Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 400, "Bad request error.");
                done();
            }
        });
    });
});

//Test Cases of Delete Journal API.
describe('Test Cases of Delete Journal API', function(){
    this.timeout(20000);

    it("It should return 208 if param missing", (done)=>{
        request.post({url : URL+'/onelife/deletejournal', headers : {access_token : access_token}, form : {journalId : ''}}, (err, response)=>{
            if(err){
                done("Error in Journal Delete API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 208, "User Journal ID missing");
                done();
            }
        });
    });

    it("It should return 401 if access token is invalid", (done)=>{
        request.post({url : URL+'/onelife/deletejournal', headers : {access_token : 'kjdfngkjnsdlk'}, form : {journalId : user_journalid}}, (err, response)=>{
            if(err){
                done("Error in Journal Delete API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "You are not authorize user");
                done();
            }
        });
    });

    it("It should return 200 if journal delete successfully", (done)=>{
        request.post({url : URL+'/onelife/deletejournal', headers : {access_token : access_token}, form : {journalId : user_journalid}}, (err, response)=>{
            if(err){
                done("Error in Delete Journal API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Journal delete successfully");
                done();
            }
        });
    });

    it("It should return 206 if if there is no record to delete", (done)=>{
        request.post({url : URL+'/onelife/deletejournal', headers : {access_token : access_token}, form : {journalId : user_journalid}}, (err, response)=>{
            if(err){
                done("Error in Journal Delete API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 206, "No record found");
                done();
            }
        });
    }); 
});

//Test Cases of Add to Favorite API.
describe('Test Cases of Add to Favorite API', function(){
    this.timeout(20000);
    it("It should return 200 if food add to favorite successfully", (done)=>{
        request.post({url : URL+'/onelife/getfoodentries', headers : {access_token : access_token}, form : {id : 200, foodId : 662, favorite : 1}}, (err, response)=>{
            if(err){
                done("Error in Add to Favorite API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Favourite Food Updated Sucessfully");
                done();
            }
        });
    });

        it("It should return 200 if food remove from favorite successfully", (done)=>{
        request.post({url : URL+'/onelife/getfoodentries', headers : {access_token : access_token}, form : {id : 200, foodId : 662, favorite : 0}}, (err, response)=>{
            if(err){
                done("Error in Remove from Favorite API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Favorite food updated successfully");
                done();
            }
        });
    });
});

// Test Cases for Add Custom Food API
describe('Test Cases of Add Custom Food API', function(){
    this.timeout(20000);
    it("It should return 200 if custom food added successfully", (done)=>{
        request.post({url : URL+'/onelife/addcustom', headers : {access_token : access_token}, form : {category_Id : 3, food_name : 'Shashlik', servingsize_id : 1, food_blocks : 1, free_food : false, calories : 50, proteins : 2.5, carbs : 0, is_public : 1, serving_size : 3.5, subCategory : 'sub', protein_Only : 1, keyword : 'sha', user_id : user_id, DValue : true}}, (err, response)=>{
            if(err){
                done("Error in Add Custome Food API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Custom Food added successfully");
                done();
            }
        });
    });
});

// Test Case for Get Custom Food API
describe('Test Cases of Get Custom Food API', function(){
    this.timeout(20000);
    it("It should return 200 if Custom Food get successfully", (done)=>{
        request.post({url : URL+'/onelife/getfoodentries', headers : {access_token : access_token}, form : {value : 'sha', id : user_id, category : 'self'}}, (err, response)=>{
            if(err){
                done("Error in Get Food Enteries API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                custom_foodid = res.response[0].FoodEntryID;
                assert.equal(res.responseCode, 200, "Custom Food get successfully");
                done();
            }
        });
    });
});    
// Test Cases for Delete Custom Food API
describe('Test Cases of Delete Custom Food API', function(){
    this.timeout(20000);
    it("It should return 200 if custom food deleted successfully", (done)=>{
        request.post({url : URL+'/onelife/deletecustomfood', headers : {access_token : access_token}, form : {foodId : custom_foodid}}, (err, response)=>{
            if(err){
                done("Error in Delete Custome Food API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Custom Food deleted successfully");
                done();
            }
        });
    });
});

//Test Cases of Logout API.
describe('Test Cases of Logout API', function(){
    this.timeout(20000);
    it("It should return 401 if user cannot logout successfully.", (done)=>{
        request.post({url : URL+'/onelife/signout', headers : {access_token : 'rghkjnskjgfnknkfnlke'}}, (err, response)=>{
            if(err){
                done("Error in Logout API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "You are not authorize user");
                done();
            }
        });
    });
    it("It should return 200 if user Logout successfully.", (done)=>{
        request.post({url : URL+'/onelife/signout', headers : {access_token : access_token}}, (err, response)=>{
            if(err){
                done("Error in Logout API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Logout successfully");
                done();
            }
        });
    });
});

//Test Cases of Logout API.
describe('Test Cases of Logout API', function(){
    this.timeout(20000);
    it("It should return 401 if user cannot logout successfully.", (done)=>{
        request.post({url : URL+'/onelife/signout', headers : {access_token : 'rghkjnskjgfnknkfnlke'}}, (err, response)=>{
            if(err){
                done("Error in Logout API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 401, "You are not authorize user");
                done();
            }
        });
    });
    it("It should return 200 if user Logout successfully.", (done)=>{
        request.post({url : URL+'/onelife/signout', headers : {access_token : access_token}}, (err, response)=>{
            if(err){
                done("Error in Logout API Test Case", err);
            }
            else{
                let res = JSON.parse(response.body);
                assert.equal(res.responseCode, 200, "Logout successfully");
                done();
            }
        });
    });
});



